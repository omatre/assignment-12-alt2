﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PostGrad.Data.Entities;
using PostGrad.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostGrad.Domain
{
    /// <summary>
    /// Static class with one method SerializeToJSON which returns a JSON-string
    /// with all Professor-objects in the database.
    /// </summary>
    public static class JSONProfessor
    {
        private static PostGradDbContext context = new PostGradDbContext();

        /// <summary>
        /// Serializes Professor-objects from database
        /// </summary>
        /// <returns>JSON-string with Professor -> Course -> StudentCourse -> Student</returns>
        public static string SerializeToJSON()
        {
            var professors = context.Professors
                .Include(p => p.Course)
                .ThenInclude(s => s.StudentCourses)
                .ThenInclude(x => x.Student)
                .ToList();

            string json = JsonConvert.SerializeObject(professors, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            return json;
        }
    }
}
