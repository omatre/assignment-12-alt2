﻿using Microsoft.EntityFrameworkCore.Internal;
using PostGrad.Data.Entities;
using PostGrad.Data.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostGrad.Domain.Repositories
{
    public class StudentCourseRepository
    {
        private readonly PostGradDbContext context;

        public StudentCourseRepository()
        {
            context = new PostGradDbContext();
        }

        /// <summary>
        /// Adds a connection between a Student and a Course to make a new StudentCourse.
        /// </summary>
        /// <param name="student">Student to be connected</param>
        /// <param name="course">Course to be connected</param>
        /// <returns>Boolean flag</returns>
        public bool Add(Student student, Course course)
        {
            if (context.StudentCourses.ToList()
                .Any(t => t.StudentId == student.Id && t.CourseId == course.Id))
            {
                return false;
            }

            context.StudentCourses.Add(new StudentCourse
            {
                StudentId = student.Id,
                CourseId = course.Id
            });

            context.SaveChanges();

            return true;
        }
    }
}
