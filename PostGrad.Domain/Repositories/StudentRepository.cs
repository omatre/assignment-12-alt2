﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using PostGrad.Data.Entities;
using PostGrad.Data.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostGrad.Domain.Repositories
{
    public class StudentRepository
    {
        private readonly PostGradDbContext context;

        public StudentRepository()
        {
            context = new PostGradDbContext();
        }

        //Create CRUD-operations
        /// <summary>
        /// Creates a new Student with given parametres and saves it to database.
        /// </summary>
        /// <param name="firstname">First name</param>
        /// <param name="lastname">Last name</param>
        /// <param name="email">E-mail address</param>
        /// <param name="dateOfBirth">Date of Birth</param>
        /// <returns>Created Student</returns>
        public Student Create(string firstname, string lastname, 
            string email, DateTime dateOfBirth)
        {
            if (context.Students.Any(t => t.Email == email))
            {
                return context.Students.Where(t => t.Email == email).Single();
            }

            var student = new Student
            {
                FirstName = firstname,
                LastName = lastname,
                Email = email,
                DateOfBirth = dateOfBirth,
                DateOfEnroll = DateTime.Now.Date
            };

            context.Students.Add(student);

            context.SaveChanges();

            return student;
        }

        /// <summary>
        /// Adds a new Course to Student given by ID.
        /// </summary>
        /// <param name="studentId">ID of Student</param>
        /// <param name="course">Course to connect to Student</param>
        /// <returns>Boolean flag</returns>
        public bool AddCourse(int studentId, Course course)
        {
            var student = Read(studentId);

            if (student.StudentCourses.Any(t => t.CourseId == course.Id && t.StudentId == studentId))
            {
                return false;
            }

            var studCourseRepo = new StudentCourseRepository();
            studCourseRepo.Add(student, course);

            return true;
        }

        /// <summary>
        /// Adds a range of Course-objects to Student with given ID.
        /// </summary>
        /// <param name="studentId">ID of Student</param>
        /// <param name="courses">Collection of Course-objects</param>
        /// <returns>Boolean flag</returns>
        public bool AddCourseRange(int studentId, ICollection<Course> courses)
        {
            bool flag = false;

            var student = Read(studentId);

            foreach (var course in courses)
            {
                if (student.StudentCourses.Any(t => t.CourseId != course.Id || t.StudentId != studentId))
                {
                    var studCourseRepo = new StudentCourseRepository();
                    studCourseRepo.Add(student, course);

                    flag = true;
                }
            }

            return flag;
        }

        /// <summary>
        /// Gets all Student-objects from database.
        /// </summary>
        /// <returns>List of Student-objects connected with StudentCourse-objects and Course-objects</returns>
        public ICollection<Student> ReadAll()
        {
            if (context.Students.Count() == 0)
            {
                return null;
            }

            return context.Students
                .Include(s => s.StudentCourses)
                .ThenInclude(t => t.Course).ToList();
        }

        /// <summary>
        /// Gets Student-objects containing or matching with search string in database.
        /// </summary>
        /// <param name="search">Search string</param>
        /// <returns>List of Student-objects connected with StudentCourse-objects and Course-objects</returns>
        public ICollection<Student> Read(string search)
        {
            var students = from element in context.Students
                           .Include(s => s.StudentCourses)
                           .ThenInclude(t => t.Course)
                           where
                                ($"{element.FirstName} {element.LastName}").Contains(search) ||
                                ($"{element.FirstName} {element.LastName}").Equals(search)
                             select element;

            return students.ToList();
        }

        /// <summary>
        /// Gets Student with given ID.
        /// </summary>
        /// <param name="id">ID of Student</param>
        /// <returns>Student-object with matching ID in database</returns>
        public Student Read(int id)
        {
            var student = context.Students
                           .Include(s => s.StudentCourses)
                           .ThenInclude(t => t.Course).ToList()[id];

            if (student == null)
            {
                throw new Exception($"Cannot find student with id { id }");
            }

            return student;
        }

        /// <summary>
        /// Gets Course-objects that matches the Student-ID and are filtered through the Predicate.
        /// </summary>
        /// <param name="studentId">ID of Student</param>
        /// <param name="predicate">Predicate to filter Course-objects</param>
        /// <returns>List of Course-objects</returns>
        public ICollection<Course> GetCourses(int studentId, Predicate<Course> predicate)
        {
            var coursesUnfiltered = from element in context.StudentCourses
                                    where element.StudentId == studentId
                                    select element.Course;

            var coursesFiltered = new List<Course>();

            foreach (var course in coursesUnfiltered)
            {
                if (predicate.Invoke(course))
                {
                    coursesFiltered.Add(course);
                }
            }

            return coursesFiltered;
        }

        /// <summary>
        /// Updates data on Student with matching e-mail address with data from passed Student-object.
        /// </summary>
        /// <param name="email">E-mail address</param>
        /// <param name="newStudent">New Student</param>
        /// <returns>Boolean flag</returns>
        public bool Update(string email, Student newStudent)
        {
            var flag = false;

            var oldStudent = context.Students.FirstOrDefault(t => t.Email == email);

            if (oldStudent != null)
            {
                oldStudent.FirstName = newStudent.FirstName;
                oldStudent.LastName = newStudent.LastName;
                oldStudent.Email = newStudent.Email;
                oldStudent.DateOfBirth = newStudent.DateOfBirth;
                oldStudent.DateOfEnroll = newStudent.DateOfEnroll;

                context.SaveChanges();

                flag = true;
            }

            return flag;
        }

        /// <summary>
        /// Updates Student with given ID with info from newStudent.
        /// </summary>
        /// <param name="id">ID of Student</param>
        /// <param name="newStudent">New Student</param>
        /// <returns>Boolean flag</returns>
        public bool Update(int id, Student newStudent)
        {
            var flag = false;

            var oldStudent = context.Students.FirstOrDefault(t => t.Id == id);

            if (oldStudent != null)
            {
                oldStudent.FirstName = newStudent.FirstName;
                oldStudent.LastName = newStudent.LastName;
                oldStudent.Email = newStudent.Email;
                oldStudent.DateOfBirth = newStudent.DateOfBirth;
                oldStudent.DateOfEnroll = newStudent.DateOfEnroll;

                context.SaveChanges();

                flag = true;
            }

            return flag;
        }

        /// <summary>
        /// Deletes Student with given e-mail address.
        /// </summary>
        /// <param name="email">E-mail address</param>
        /// <returns>Boolean flag</returns>
        public bool Delete(string email)
        {
            var flag = false;

            var student = context.Students.FirstOrDefault(t => t.Email == email);

            if (student != null)
            {
                context.Students.Remove(student);

                context.SaveChanges();

                flag = true;
            }

            return flag;
        }

        /// <summary>
        /// Deletes Student with matching ID.
        /// </summary>
        /// <param name="id">ID of Student</param>
        /// <returns>Boolean flag</returns>
        public bool Delete(int id)
        {
            var flag = false;

            var student = context.Students.FirstOrDefault(t => t.Id == id);

            if (student != null)
            {
                var studCourses = context.StudentCourses.Where(t => t.StudentId == id);

                context.StudentCourses.RemoveRange(studCourses);
                context.Students.Remove(student);

                context.SaveChanges();

                flag = true;
            }

            return flag;
        }
    }
}
