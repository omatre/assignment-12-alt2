﻿using PostGrad.Data.Entities;
using PostGrad.Data.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostGrad.Domain.Repositories
{
    public class ProfessorRepository
    {
        private readonly PostGradDbContext context;

        public ProfessorRepository()
        {
            context = new PostGradDbContext();
        }

        //Create CRUD-operations
        /// <summary>
        /// Creates a new Professor-object using given parametres and saves it to database if
        /// database not already containing Professor with given email.
        /// </summary>
        /// <param name="firstname">First name</param>
        /// <param name="lastname">Last name</param>
        /// <param name="email">E-mail address</param>
        /// <param name="dateOfBirth">Date of birth</param>
        /// <returns>Professor saved to database</returns>
        public Professor Create(string firstname, string lastname,
            string email, DateTime dateOfBirth)
        {
            if (context.Professors.Any(t => t.Email == email))
            {
                return null;
            }

            var professor = new Professor
            {
                FirstName = firstname,
                LastName = lastname,
                Email = email,
                DateOfBirth = dateOfBirth,
                DateHired = DateTime.Now.Date
            };

            context.Professors.Add(professor);

            context.SaveChanges();

            return professor;
        }

        /// <summary>
        /// Gets all Professor-objects from database if any.
        /// </summary>
        /// <returns>List of Professor-objects</returns>
        public ICollection<Professor> ReadAll()
        {
            if (context.Professors.Count() == 0)
            {
                return null;
            }

            return context.Professors.ToList();
        }

        /// <summary>
        /// Gets all Professor-objects in the database matching or containing the search string.
        /// </summary>
        /// <param name="search">Search string</param>
        /// <returns>List of Professor-objects</returns>
        public ICollection<Professor> Read(string search)
        {
            var professors = from element in context.Professors
                             where
                                ($"{element.FirstName} {element.LastName}").Contains(search) ||
                                ($"{element.FirstName} {element.LastName}").Equals(search)
                             select element;

            return professors.ToList();
        }

        /// <summary>
        /// Gets Professor with given ID.
        /// </summary>
        /// <param name="id">ID of Professor</param>
        /// <returns>Professor matching the ID</returns>
        public Professor Read(int id)
        {
            var professor = context.Professors.Find(id);

            return professor;
        }

        /// <summary>
        /// Gets all Course-objects with the given Professor-ID that matches the predicate.
        /// </summary>
        /// <param name="professorId">ID of Professor</param>
        /// <param name="predicate">Predicate to filter Course-objects</param>
        /// <returns></returns>
        public ICollection<Course> GetCourses(int professorId, Predicate<Course> predicate)
        {
            var coursesUnfiltered = from element in context.Courses
                                    where element.ProfessorId == professorId
                                    select element;

            var coursesFiltered = new List<Course>();

            foreach (var course in coursesUnfiltered)
            {
                if (predicate.Invoke(course))
                {
                    coursesFiltered.Add(course);
                }
            }

            return coursesFiltered;
        }

        /// <summary>
        /// Updates info on Professor with given email-address with info from new Professor.
        /// </summary>
        /// <param name="email">E-mail address to find Professor</param>
        /// <param name="newProfessor">New Professor</param>
        /// <returns>Boolean flag</returns>
        public bool Update(string email, Professor newProfessor)
        {
            var flag = false;

            var oldProfessor = context.Professors.FirstOrDefault(t => t.Email == email);

            if (oldProfessor != null)
            {
                oldProfessor.FirstName = newProfessor.FirstName;
                oldProfessor.LastName = newProfessor.LastName;
                oldProfessor.Email = newProfessor.Email;
                oldProfessor.DateOfBirth = newProfessor.DateOfBirth;
                oldProfessor.DateHired = newProfessor.DateHired;

                context.SaveChanges();

                flag = true;
            }

            return flag;
        }

        /// <summary>
        /// Updates Professor with given id using data from newProfessor.
        /// </summary>
        /// <param name="id">ID of Professor</param>
        /// <param name="newProfessor">New Professor</param>
        /// <returns>Boolean flag</returns>
        public bool Update(int id, Professor newProfessor)
        {
            var flag = false;

            var oldProfessor = context.Professors.FirstOrDefault(t => t.Id == id);

            if (oldProfessor != null)
            {
                oldProfessor.FirstName = newProfessor.FirstName;
                oldProfessor.LastName = newProfessor.LastName;
                oldProfessor.Email = newProfessor.Email;
                oldProfessor.DateOfBirth = newProfessor.DateOfBirth;
                oldProfessor.DateHired = newProfessor.DateHired;

                context.SaveChanges();

                flag = true;
            }

            return flag;
        }

        /// <summary>
        /// Deletes Professor with given e-mail address.
        /// </summary>
        /// <param name="email">E-mail address of Professor to be deleted</param>
        /// <returns>Boolean flag</returns>
        public bool Delete(string email)
        {
            var flag = false;

            var professor = context.Professors.FirstOrDefault(t => t.Email == email); ;

            if (professor != null)
            {
                context.Professors.Remove(professor);

                context.SaveChanges();

                flag = true;
            }

            return flag;
        }

        /// <summary>
        /// Deletes Professor with given ID.
        /// </summary>
        /// <param name="id">ID of Professor</param>
        /// <returns>Boolean flag</returns>
        public bool Delete(int id)
        {
            var flag = false;

            var professor = context.Professors.FirstOrDefault(t => t.Id == id);

            if (professor != null)
            {
                context.Professors.Remove(professor);

                context.SaveChanges();

                flag = true;
            }

            return flag;
        }
    }
}
