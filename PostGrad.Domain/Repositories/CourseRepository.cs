﻿using Microsoft.EntityFrameworkCore;
using PostGrad.Data.Entities;
using PostGrad.Data.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostGrad.Domain.Repositories
{
    public class CourseRepository
    {
        private readonly PostGradDbContext context;

        public CourseRepository()
        {
            context = new PostGradDbContext();
        }

        //Create CRUD-operations
        /// <summary>
        /// Creates a Course-object using parameter data.
        /// </summary>
        /// <param name="name">Name of course</param>
        /// <param name="startDate">Start date of course</param>
        /// <returns>A copy of the Course created</returns>
        public Course Create(string name, DateTime startDate)
        {
            if (context.Courses.Any(t => t.Name == name && t.StartDate == startDate))
            {
                return null;
            }

            var course = new Course
            {
                Name = name,
                StartDate = startDate
            };

            context.Courses.Add(course);

            context.SaveChanges();

            return course;
        }

        /// <summary>
        /// Assigns professor to given Course.
        /// </summary>
        /// <param name="courseId">ID of Course</param>
        /// <param name="professor">Professor-object to be assigned</param>
        /// <returns>Boolean flag to tell the user wether the assignment succeeded</returns>
        public bool AssignProfessor(int courseId, Professor professor)
        {
            Course course = Read(courseId);

            if (course.Professor == null)
            {
                //Stops the else-if checking a null-object
            }
            else if (course.Professor.Email == professor.Email)
            {
                return false;
            }

            course.Professor = professor;

            context.SaveChanges();

            return true;
        }

        /// <summary>
        /// Adds Student to a given Course.
        /// </summary>
        /// <param name="courseId">ID of Course</param>
        /// <param name="student">Student to be added to the Course</param>
        /// <returns></returns>
        public bool AddStudent(int courseId, Student student)
        {
            var course = Read(courseId);

            if (course.StudentCourses.Any(t => t.CourseId == courseId && t.StudentId == student.Id))
            {
                return false;
            }

            var studCourseRepo = new StudentCourseRepository().Add(student, course);

            return true;
        }

        /// <summary>
        /// Adds multiple Student-objects to given Course.
        /// </summary>
        /// <param name="courseId">ID of Course</param>
        /// <param name="students">Collection of students to be added</param>
        /// <returns>Boolean flag</returns>
        public bool AddStudentRange(int courseId, ICollection<Student> students)
        {
            bool flag = false;

            var course = Read(courseId);

            foreach (var student in students)
            {
                if (course.StudentCourses.Any(t => t.CourseId != courseId || t.StudentId != student.Id))
                {
                    var studCourseRepo = new StudentCourseRepository().Add(student, course);

                    flag = true;
                }
            }

            return flag;
        }

        /// <summary>
        /// Gets all the Course-objects in database including the connected StudentCourse-objects through to Student-objects.
        /// </summary>
        /// <returns>A Collection of Course-objects</returns>
        public ICollection<Course> ReadAll()
        {
            if (context.Courses.Count() == 0)
            {
                return null;
            }

            return context.Courses
                .Include(s => s.StudentCourses)
                .ThenInclude(t => t.Student).ToList();
        }

        /// <summary>
        /// Gets all the Course-objects containing or matching with the search string in database.
        /// </summary>
        /// <param name="search">Search string</param>
        /// <returns>List of Course-objects</returns>
        public ICollection<Course> Read(string search)
        {
            var courses = from element in context.Courses
                .Include(s => s.StudentCourses)
                .ThenInclude(t => t.Student).ToList()
                          where
                             (element.Name.Contains(search)) ||
                             (element.Name.Equals(search))
                          select element;

            return courses.ToList();
        }

        /// <summary>
        /// Gets the Course-object with given ID.
        /// </summary>
        /// <param name="id">ID of Course</param>
        /// <returns>Course-object</returns>
        public Course Read(int id)
        {
            var course = context.Courses
                .Include(s => s.StudentCourses)
                .ThenInclude(t => t.Student)
                .Single(x => x.Id == id);

            if (course == null)
            {
                throw new Exception($"Cannot find course with courseId { id }");
            }

            return course;
        }

        /// <summary>
        /// Gets Student-objects connected to Course with given ID that matches a predicate.
        /// </summary>
        /// <param name="courseId">ID of Course</param>
        /// <param name="predicate">Predicate to match Student-objects</param>
        /// <returns>List of Student-objects</returns>
        public ICollection<Student> GetStudents(int courseId, Predicate<Student> predicate)
        {
            var studentsUnfiltered = from element in context.StudentCourses
                                     where element.CourseId == courseId
                                     select element.Student;

            var studentsFiltered = new List<Student>();

            foreach (var student in studentsUnfiltered)
            {
                if (predicate.Invoke(student))
                {
                    studentsFiltered.Add(student);
                }
            }

            return studentsFiltered;
        }

        /// <summary>
        /// Gets the Professor-object assigned to Course with given ID.
        /// </summary>
        /// <param name="courseId">ID of Course</param>
        /// <returns>Professor-object</returns>
        public Professor GetProfessor(int courseId)
        {
            var professor = Read(courseId).Professor;

            if (professor == null)
            {
                throw new Exception("Course with courseId { courseId } has no professor assigned");
            }

            return professor;
        }

        /// <summary>
        /// Updates Name and StartDate property in course with id
        /// with properties from newCourse.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newCourse"></param>
        /// <returns>True if course was updated, false if not</returns>
        public bool Update(int id, Course newCourse)
        {
            var flag = false;

            var oldCourse = context.Courses.FirstOrDefault(t => t.Id == id);

            if (oldCourse != null)
            {
                oldCourse.Name = newCourse.Name;
                oldCourse.StartDate = newCourse.StartDate;

                context.SaveChanges();

                flag = true;
            }

            return flag;
        }

        /// <summary>
        /// Delete Course with given ID.
        /// </summary>
        /// <param name="id">ID of Course</param>
        /// <returns>Boolean flag</returns>
        public bool Delete(int id)
        {
            var flag = false;

            var course = context.Courses.FirstOrDefault(t => t.Id == id);

            if (course != null)
            {
                var studentCourses = context.StudentCourses.Where(t => t.CourseId == id).ToList();

                context.StudentCourses.RemoveRange(studentCourses);
                context.Courses.Remove(course);

                context.SaveChanges();

                flag = true;
            }

            return flag;
        }
    }
}
