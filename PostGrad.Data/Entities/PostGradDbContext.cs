﻿using Microsoft.EntityFrameworkCore;
using PostGrad.Data.Entities.Models;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace PostGrad.Data.Entities
{
    public class PostGradDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Professor> Professors { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<StudentCourse> StudentCourses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=PC7596\SQLEXPRESS;" +
                                         "Initial Catalog=PostGradDB;" +
                                         "Integrated Security=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Entity-key for many-to-many relationsship between students and courses
            modelBuilder.Entity<StudentCourse>().HasKey(c => new { c.StudentId, c.CourseId });

            //Connects Course with StudentCourse
            modelBuilder.Entity<StudentCourse>()
                .HasOne(x => x.Course)
                .WithMany(y => y.StudentCourses)
                .HasForeignKey(z => z.CourseId);

            //Connects Student with StudentCourse
            modelBuilder.Entity<StudentCourse>()
                .HasOne(x => x.Student)
                .WithMany(y => y.StudentCourses)
                .HasForeignKey(z => z.StudentId);
        }
    }
}
