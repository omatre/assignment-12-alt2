﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostGrad.Data.Entities.Models
{
    public abstract class Person
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}
