﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PostGrad.Data.Entities.Models
{
    public class Course
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public int? ProfessorId { get; set; }
        public Professor? Professor { get; set; }
        public ICollection<StudentCourse>? StudentCourses { get; set; }
    }
}
