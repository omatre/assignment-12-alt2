﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostGrad.Data.Entities.Models
{
    /// <summary>
    /// Model to handle many-to-many relationsship between courses and students.
    /// </summary>
    public class StudentCourse
    {
        //Properties for IDs to avoid name collision in key-creation(OnModelCreation())
        public Student Student { get; set; }
        public int StudentId { get; set; }
        public Course Course { get; set; }
        public int CourseId { get; set; }
    }
}
