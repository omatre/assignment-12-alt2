﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostGrad.Data.Entities.Models
{
    public class Student : Person
    {
        public DateTime? DateOfEnroll { get; set; }
        public ICollection<StudentCourse>? StudentCourses { get; set; }
    }
}
