﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PostGrad.Data.Entities.Models
{
    public class Professor : Person
    {
        public DateTime? DateHired { get; set; }
        public Course Course { get; set; }
    }
}
