﻿using Microsoft.VisualBasic;
using PostGrad.Data.Entities.Models;
using PostGrad.Domain;
using PostGrad.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace PostGrad.ConsoleApp
{
    class Program
    {
        //Initiating repos
        private static StudentRepository studentRepos = new StudentRepository();
        private static ProfessorRepository professorRepos = new ProfessorRepository();
        private static CourseRepository courseRepos = new CourseRepository();

        static void Main(string[] args)
        {
            StartUpWindow();

            //Loops through MainMenu until user exits
            bool showMenu = true;

            while (showMenu)
            {
                showMenu = MainMenu();
            }
        }

        private static void StartUpWindow()
        {
            Console.WriteLine("Currently, only Courses-menu and Supervisor => Display Professor (JSON) is implemented.");
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }

        private static bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("Choose an option:");
            Console.WriteLine("1: Students - Not Implemented");
            Console.WriteLine("2: Courses");
            Console.WriteLine("3: Supervisors");
            Console.WriteLine("q: Quit");

            switch (Console.ReadKey().KeyChar)
            {
                case '1':
                    StudentWindow();
                    return true;
                case '2':
                    CourseWindow();
                    return true;
                case '3':
                    ProfessorWindow();
                    return true;
                case 'q':
                    return false;
                default:
                    return true;
            }
        }

        private static void ProfessorWindow()
        {
            bool showWindow = true;

            while (showWindow)
            {
                Console.Clear();
                Console.WriteLine("Choose an option:");
                Console.WriteLine("1: Create Professor - Not Implemented");
                Console.WriteLine("2: Display Professor");
                Console.WriteLine("3: Update Professor - Not Implemented");
                Console.WriteLine("4: Delete Professor - Not Implemented");
                Console.WriteLine("b: Back");

                switch (Console.ReadKey().KeyChar)
                {
                    case '1':
                        CreateProfessor();
                        break;
                    case '2':
                        DisplayProfessor();
                        break;
                    case '3':
                        UpdateProfessor();
                        break;
                    case '4':
                        DeleteProfessor();
                        break;
                    case 'b':
                        showWindow = false;
                        break;
                    default:
                        break;
                }
            }
        }

        private static void CourseWindow()
        {
            bool showWindow = true;

            while (showWindow)
            {
                Console.Clear();
                Console.WriteLine("Choose an option:");
                Console.WriteLine("1: Create Course");
                Console.WriteLine("2: Display Course");
                Console.WriteLine("3: Update Course");
                Console.WriteLine("4: Delete Course");
                Console.WriteLine("b: Back");

                switch (Console.ReadKey().KeyChar)
                {
                    case '1':
                        CreateCourse();
                        break;
                    case '2':
                        DisplayCourse();
                        break;
                    case '3':
                        UpdateCourse();
                        break;
                    case '4':
                        DeleteCourse();
                        break;
                    case 'b':
                        showWindow = false;
                        break;
                    default:
                        break;
                }
            }
        }

        private static void StudentWindow()
        {
            bool showWindow = true;

            while (showWindow)
            {
                Console.Clear();
                Console.WriteLine("Choose an option:");
                Console.WriteLine("1: Create Student");
                Console.WriteLine("2: Display Student");
                Console.WriteLine("3: Update Student");
                Console.WriteLine("4: Delete Student");
                Console.WriteLine("b: Back");

                switch (Console.ReadKey().KeyChar)
                {
                    case '1':
                        CreateStudent();
                        break;
                    case '2':
                        DisplayStudent();
                        break;
                    case '3':
                        UpdateStudent();
                        break;
                    case '4':
                        DeleteStudent();
                        break;
                    case 'b':
                        showWindow = false;
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Creates Course-object and prompts user for data to create max one Professor-object 
        /// and limitless number of Student-objects.
        /// </summary>
        private static void CreateCourse()
        {
            try
            {
                Console.Clear();
                Console.Write("Course name: ");
                string name = Console.ReadLine();
                Console.Write("Start date (dd/mm/yyyy): ");
                string date = Console.ReadLine();

                var course = courseRepos.Create(name, DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                Console.Clear();
                Console.Write("\nDo you want to assign a professor? (y/n) ");
                bool assignProfessor = Console.ReadKey().KeyChar == 'y' ? true : false;

                if (assignProfessor)
                {
                    Console.Clear();
                    Console.Write("First Name: ");
                    string firstname = Console.ReadLine();
                    Console.Write("Last Name: ");
                    string lastname = Console.ReadLine();
                    Console.Write("Email: ");
                    string email = Console.ReadLine();
                    Console.Write("Date of Birth (dd/mm/yyyy): ");
                    string dob = Console.ReadLine();

                    var newProfessor = professorRepos.Create(firstname, lastname,
                        email, DateTime.ParseExact(dob, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                    courseRepos.AssignProfessor(course.Id, newProfessor);
                }

                Console.Clear();
                Console.Write("\nDo you want to assign a student? (y/n) ");
                bool assignStudent = Console.ReadKey().KeyChar == 'y' ? true : false;

                while (assignStudent)
                {
                    Console.Clear();
                    Console.Write("First Name: ");
                    string firstname = Console.ReadLine();
                    Console.Write("Last Name: ");
                    string lastname = Console.ReadLine();
                    Console.Write("Email: ");
                    string email = Console.ReadLine();
                    Console.Write("Date of Birth (dd/mm/yyyy): ");
                    string dob = Console.ReadLine();

                    var newStudent = studentRepos.Create(firstname, lastname,
                        email, DateTime.ParseExact(dob, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                    courseRepos.AddStudent(course.Id, newStudent);

                    Console.Clear();
                    Console.Write("\nDo you want to assign a student? (y/n) ");
                    assignStudent = Console.ReadKey().KeyChar == 'y' ? true : false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"\nError: { ex.Message }\n{ ex.StackTrace }\n{ ex.InnerException }");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Gets all the Course-objects from database and display it with connected Professor-object and Student-objects.
        /// </summary>
        private static void DisplayCourse()
        {
            Console.Clear();
            Console.WriteLine("Here are the courses:");
            var courses = courseRepos.ReadAll();

            if (courses != null)
            {
                foreach (var course in courses)
                {
                    string profName = course.ProfessorId != null ? professorRepos.Read((int)course.ProfessorId).LastName : "Nobody";

                    Console.WriteLine($"{ course.Id }: { course.Name }, first day " +
                        $"{ course.StartDate.ToString("dd.MMMM yyyy") } with Prof.{ profName }");

                    foreach (var studentCourse in course.StudentCourses)
                    {
                        var student = studentCourse.Student;

                        if (student != null)
                        {
                            Console.WriteLine($"\t{ student.Id }: { student.FirstName } { student.LastName }, " +
                                $"{ student.Email } born { student.DateOfBirth }, enrolled { student.DateOfEnroll }");
                        }
                    }
                }
            }

            Console.WriteLine("\nPress any key...");
            Console.ReadKey();
        }

        /// <summary>
        /// Updates the Course-Name and Course-StartDate in a chosen Course-object.
        /// </summary>
        private static void UpdateCourse()
        {
            DisplayCourse();
            Console.Write("\nChoose a course by ID: ");
            int id = int.Parse(Console.ReadLine());
            Console.Write("New Name: ");
            string name = Console.ReadLine();
            Console.Write("New Start Date: ");
            string dateString = Console.ReadLine();
            DateTime date = DateTime.ParseExact(dateString, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var newCourse = new Course { Name = name, StartDate = date };
            courseRepos.Update(id, newCourse);
        }

        /// <summary>
        /// Delete a Course-object and connected StudentCourse-objects.
        /// </summary>
        private static void DeleteCourse()
        {
            DisplayCourse();
            Console.Write("\nChoose a course by ID: ");
            int id = int.Parse(Console.ReadLine());
            courseRepos.Delete(id);
        }

        private static void DeleteStudent()
        {
            Console.Clear();
            Console.WriteLine("Method is not implemented.");
        }

        private static void UpdateStudent()
        {
            Console.Clear();
            Console.WriteLine("Method is not implemented.");
        }

        private static void DisplayStudent()
        {
            Console.Clear();
            Console.WriteLine("Method is not implemented.");
        }

        private static void CreateStudent()
        {
            Console.Clear();
            Console.WriteLine("Method is not implemented.");
        }

        private static void DeleteProfessor()
        {
            Console.Clear();
            Console.WriteLine("Method is not implemented.");
        }

        private static void UpdateProfessor()
        {
            Console.Clear();
            Console.WriteLine("Method is not implemented.");
        }

        /// <summary>
        /// Displays JSON-serialized Professor-objects.
        /// </summary>
        private static void DisplayProfessor()
        {
            Console.Clear();
            Console.WriteLine(JSONProfessor.SerializeToJSON());
            Console.ReadKey();
        }

        private static void CreateProfessor()
        {
            Console.Clear();
            Console.WriteLine("Method is not implemented.");
        }
    }
}
